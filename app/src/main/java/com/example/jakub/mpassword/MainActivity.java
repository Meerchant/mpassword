package com.example.jakub.mpassword;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    public String password;
    public boolean first;
    public boolean second = false;
    public int magicnumber;


    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences("mycompany.note", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();


        magicnumber = sharedPreferences.getInt("code", 3);
        first = sharedPreferences.getBoolean("first", true);
        password = sharedPreferences.getString("pin", "");

        if (first) {


            TextView tx = (TextView) findViewById(R.id.textView7);
            tx.setText("Set your new PIN");

            Random generator = new Random();
            int i = generator.nextInt(15);

            magicnumber = i;

            editor.putInt("code", i);
            editor.apply();

        }

    }


    public void enter(View view) {


        if (first) {

            TextView tx = (TextView) findViewById(R.id.textView7);
            EditText et = (EditText) findViewById(R.id.editText);
            tx.setText("Repeat your new PIN");
            password = et.getText().toString();
            et.setText("");
            second = true;
            first = false;

        } else if (second) {

            EditText et = (EditText) findViewById(R.id.editText);
            if (password.equals(et.getText().toString())) {

                Context context = getApplicationContext();
                CharSequence text = "PIN succesfully created";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                editor.putString("pin", password);
                editor.commit();

                first = false;
                editor.putBoolean("first", first);
                editor.commit();
                second = false;

            } else {

                Context context = getApplicationContext();
                CharSequence text = "PIN are  different";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                TextView tx = (TextView) findViewById(R.id.textView7);
                tx.setText("Set your new PIN");
                et.setText("");
                second = false;
                first = true;


            }

        }
        if (!(second)) {

            EditText editText = (EditText) findViewById(R.id.editText);
            String passwordw = editText.getText().toString();


            if (password.equals(passwordw)) {

                Intent i = new Intent(MainActivity.this, Menu.class);
                startActivity(i);

                editText.setText("");
            } else {

                Toast.makeText(getApplicationContext(), "Incorrect PIN", Toast.LENGTH_SHORT).show();
                editText.setText("");

            }

        }

    }


}
