package com.example.jakub.mpassword;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.Toast;

/**
 * Created by Jakub on 2018-04-22.
 */
public class Menu extends MainActivity {


    public boolean press = false;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    String pas1;
    String pas2;
    String pas3;
    String pas4;
    String pas5;
    String pas6;

    String t1;
    String t2;
    String t3;
    String t4;
    String t5;
    String t6;

    String pasSH;
    String titSH;


    ClipboardManager clipboardManager;
    ClipData clipData;
    static int whichbutton = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu2);


        Button button1 = (Button) findViewById(R.id.button1);
        Button button2 = (Button) findViewById(R.id.button2);
        Button button3 = (Button) findViewById(R.id.button3);
        Button button4 = (Button) findViewById(R.id.button4);
        Button button5 = (Button) findViewById(R.id.button5);
        Button button6 = (Button) findViewById(R.id.button6);

        sharedPreferences = getSharedPreferences("mycompany.note", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();


        t1 = sharedPreferences.getString("T1", "");
        t2 = sharedPreferences.getString("T2", "");
        t3 = sharedPreferences.getString("T3", "");
        t4 = sharedPreferences.getString("T4", "");
        t5 = sharedPreferences.getString("T5", "");
        t6 = sharedPreferences.getString("T6", "");

        pas1 = sharedPreferences.getString("PAS1", "");
        pas2 = sharedPreferences.getString("PAS2", "");
        pas3 = sharedPreferences.getString("PAS3", "");
        pas4 = sharedPreferences.getString("PAS4", "");
        pas5 = sharedPreferences.getString("PAS5", "");
        pas6 = sharedPreferences.getString("PAS6", "");


        pas1 = decode(pas1);
        pas2 = decode(pas2);
        pas3 = decode(pas3);
        pas4 = decode(pas4);
        pas5 = decode(pas5);
        pas6 = decode(pas6);

        clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        button1.setText(t1);
        button2.setText(t2);
        button3.setText(t3);
        button4.setText(t4);
        button5.setText(t5);
        button6.setText(t6);


    }


    public void p1(View v) {


        Button button1 = (Button) findViewById(R.id.button1);

        if (!(button1.getText().equals(""))) {
            clipData = ClipData.newPlainText("text", pas1);
            clipboardManager.setPrimaryClip(clipData);
            Toast.makeText(getApplicationContext(), "Password copied", Toast.LENGTH_SHORT).show();
        }
    }


    public void p2(View v) {

        Button button2 = (Button) findViewById(R.id.button2);

        if (!(button2.getText().equals(""))) {
            clipData = ClipData.newPlainText("text", pas2);
            clipboardManager.setPrimaryClip(clipData);
            Toast.makeText(getApplicationContext(), "Password copied", Toast.LENGTH_SHORT).show();
        }

    }

    public void p3(View v) {

        Button button3 = (Button) findViewById(R.id.button3);

        if (!(button3.getText().equals(""))) {
            clipData = ClipData.newPlainText("text", pas3);
            clipboardManager.setPrimaryClip(clipData);
            Toast.makeText(getApplicationContext(), "Password copied", Toast.LENGTH_SHORT).show();
        }

    }

    public void p4(View v) {

        Button button4 = (Button) findViewById(R.id.button4);

        if (!(button4.getText().equals(""))) {
            clipData = ClipData.newPlainText("text", pas4);
            clipboardManager.setPrimaryClip(clipData);
            Toast.makeText(getApplicationContext(), "Password copied", Toast.LENGTH_SHORT).show();
        }

    }

    public void p5(View v) {

        Button button5 = (Button) findViewById(R.id.button5);

        if (!(button5.getText().equals(""))) {
            clipData = ClipData.newPlainText("text", pas5);
            clipboardManager.setPrimaryClip(clipData);
            Toast.makeText(getApplicationContext(), "Password copied", Toast.LENGTH_SHORT).show();
        }

    }

    public void p6(View v) {

        Button button6 = (Button) findViewById(R.id.button6);

        if (!(button6.getText().equals(""))) {
            clipData = ClipData.newPlainText("text", pas6);
            clipboardManager.setPrimaryClip(clipData);
            Toast.makeText(getApplicationContext(), "Password copied", Toast.LENGTH_SHORT).show();
        }

    }

    public void clear(View v) {

        clipData = ClipData.newPlainText("text", "");
        clipboardManager.setPrimaryClip(clipData);
        Toast.makeText(getApplicationContext(), "Clipboard cleaned", Toast.LENGTH_SHORT).show();

    }


    public void e1(View v) {

        whichbutton = 1;
        Intent i = new Intent(Menu.this, Newpassword.class);
        startActivity(i);
        this.finish();


    }


    public void e2(View v) {

        whichbutton = 2;
        Intent i = new Intent(Menu.this, Newpassword.class);
        startActivity(i);
        this.finish();


    }

    public void e3(View v) {

        whichbutton = 3;
        Intent i = new Intent(Menu.this, Newpassword.class);
        startActivity(i);
        this.finish();


    }

    public void e4(View v) {

        whichbutton = 4;
        Intent i = new Intent(Menu.this, Newpassword.class);
        startActivity(i);
        this.finish();


    }

    public void e5(View v) {

        whichbutton = 5;
        Intent i = new Intent(Menu.this, Newpassword.class);
        startActivity(i);
        this.finish();


    }

    public void e6(View v) {

        whichbutton = 6;
        Intent i = new Intent(Menu.this, Newpassword.class);
        startActivity(i);
        this.finish();


    }

    public void edit(View v)

    {


        Button b1 = (Button) findViewById(R.id.button11);
        Button b2 = (Button) findViewById(R.id.button22);
        Button b3 = (Button) findViewById(R.id.button33);
        Button b4 = (Button) findViewById(R.id.button44);
        Button b5 = (Button) findViewById(R.id.button55);
        Button b6 = (Button) findViewById(R.id.button66);


        if (press) {


            b1.setVisibility(View.INVISIBLE);
            b2.setVisibility(View.INVISIBLE);
            b3.setVisibility(View.INVISIBLE);
            b4.setVisibility(View.INVISIBLE);
            b5.setVisibility(View.INVISIBLE);
            b6.setVisibility(View.INVISIBLE);
            press = false;

        } else {

            b1.setVisibility(View.VISIBLE);
            b2.setVisibility(View.VISIBLE);
            b3.setVisibility(View.VISIBLE);
            b4.setVisibility(View.VISIBLE);
            b5.setVisibility(View.VISIBLE);
            b6.setVisibility(View.VISIBLE);

            press = true;

        }

    }

    public void exit(View v)

    {


        this.finish();


    }


    String code(String str) {
        char x[] = str.toCharArray();

        for (int i = 0; i != x.length; i++) {
            int n = x[i];
            n += magicnumber;
            x[i] = (char) n;
        }
        return new String(x);
    }


    String decode(String str) {
        char x[] = str.toCharArray();

        for (int i = 0; i != x.length; i++) {
            int n = x[i];
            n -= magicnumber;
            x[i] = (char) n;
        }
        return new String(x);
    }


}
