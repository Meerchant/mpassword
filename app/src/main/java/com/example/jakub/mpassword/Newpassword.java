package com.example.jakub.mpassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Jakub on 2018-04-26.
 */
public class Newpassword extends Menu {


    String pasword;
    String title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newpassword);


    }


    public void save(View v) {


        int f = whichbutton;

        if (f == 1) {
            titSH = "T1";
            pasSH = "PAS1";
        }
        if (f == 2) {
            titSH = "T2";
            pasSH = "PAS2";
        }
        if (f == 3) {
            titSH = "T3";
            pasSH = "PAS3";
        }
        if (f == 4) {
            titSH = "T4";
            pasSH = "PAS4";
        }
        if (f == 5) {
            titSH = "T5";
            pasSH = "PAS5";
        }
        if (f == 6) {
            titSH = "T6";
            pasSH = "PAS6";
        }


        String pr;

        EditText editText2 = (EditText) findViewById(R.id.editText2);
        title = editText2.getText().toString();

        editor.putString(titSH, title);
        editor.commit();


        EditText editText3 = (EditText) findViewById(R.id.editText3);
        EditText editText4 = (EditText) findViewById(R.id.editText4);

        pr = editText3.getText().toString();
        pasword = editText4.getText().toString();

        if (pasword.equals(pr)) {

            pasword = code(pasword);

            editor.putString(pasSH, pasword);
            editor.commit();

            Context context = getApplicationContext();
            CharSequence text = "Password saved";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

            Intent i = new Intent(Newpassword.this, Menu.class);
            startActivity(i);
            this.finish();

        } else {

            Context context = getApplicationContext();
            CharSequence text = "Passwords are different";
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();


        }


    }


    public void cancel(View view) {

        Intent i = new Intent(Newpassword.this, Menu.class);
        startActivity(i);
        this.finish();

    }
}
